# James' additional software

In no particular order, here are the additional apps and configurations I require on an ubuntu lsudo aptop.

Details are (currently) for Ubuntu 20.10 on an IBM Thinkpad T14.

## spacevim

    curl -sLf https://spacevim.org/install.sh | bash

## ohmyzshell

    sudo apt install -y zsh
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

Configs:

* `.zshrc`
* `.zsh_history`

## slack

Requires curl

    curl -O https://downloads.slack-edge.com/linux_releases/slack-desktop-4.14.0-amd64.deb
    dpkg -i slack-desktop-4.14.0-amd64.deb

Then sign in with:

 * jclark.astro@mgail.com
 * james.clark@ligo.org
 * james.clark@physics.gatech.edu

## mattermost

     curl -O https://releases.mattermost.com/desktop/4.6.2/mattermost-desktop-4.6.2-linux-amd64.deb
     dpkg -i mattermost-desktop-4.6.2-linux-amd64.deb

## wire

For family communications

    wget https://wire-app.wire.com/linux/Wire-3.24.2939_amd64.deb


## thunderbird

Comes installed, just need to recover configuration:

    scp james.clark@ldas-grid.ligo.caltech.edu:thunderbird.tar.bz2 .
    tar xjf thunderbird.tar.bz2

## docker

Guide: https://docs.docker.com/engine/install/ubuntu/

Install deps:

    sudo apt-get install \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg \
      lsb-release

Install repo key:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    echo \
      "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

Install docker:

    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io


Configure systemd:

    sudo systemctl enable docker.service
    sudo systemctl enable containerd.service

Set data path (important for small `/`) - in `/etc/docker/daemon.json`:

    {
      "data-root": "/home/docker/data"
    }

Post-installation steps: https://docs.docker.com/engine/install/linux-postinstall/

Running as `$USER`

    groupadd docker
    usermod -aG docker $USER
    # log out / in

Docker-compose:

    sudo curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

## kubectl

    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

## zoom

    wget https://zoom.us/client/latest/zoom_amd64.deb
    sudo apt install ./zoom_amd64.deb

Config: SSO -> `caltech`


## ssh keys

Create keys:

    ssh-keygen
    xclip -sel clip .ssh/id_rsa.pub

Paste to:

 * myligo.org
 * gitlab
 * github


## git / git-lfs

    sudo apt install -y git
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
    sudo apt install -y git-lfs

Then pull down the lfs objects with:

    git lfs pull

## conky

    sudo apt install -y conky
    github.com:astroclark/regolith-conky-config.git
    sudo make install
    conky-toggle

## proprietary video codecs

Allow e.g. amazon video etc:

    sudo apt install -y ubuntu-restricted-extras

## VLC

## CVMFS

See: https://computing.docs.ligo.org/guide/cvmfs/

Set up repositories:

    wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
    sudo dpkg -i ./cvmfs-release-latest_all.deb
    wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-contrib-release/cvmfs-contrib-release-latest_all.deb
    sudo dpkg -i cvmfs-contrib-release-latest_all.deb

May need `cvmfs-contrib-release_1.13.1-1_all.d` for newer ubuntu

Install cvmfs: 

    sudo apt-get update -y
    sudo apt-get install -y cvmfs cvmfs-config-osg cvmfs-x509-helper

Basic setup:

    sudo cvmfs_config setup

Configure repositories:

    $  cat /etc/cvmfs/default.local
    CVMFS_REPOSITORIES=oasis.opensciencegrid.org,gwosc.osgstorage.org,singularity.opensciencegrid.org
    CVMFS_QUOTA_LIMIT=20000
    CVMFS_HTTP_PROXY=DIRECT


Reconfigure cvmfs and probe to check they work ok:

    sudo cvmfs_config setup
    cvmfs_config probe

Add LIGO data:

    $  cat /etc/cvmfs/default.local
    CVMFS_REPOSITORIES=oasis.opensciencegrid.org,gwosc.osgstorage.org,singularity.opensciencegrid.org,ligo.osgstorage.org

Activate proxy:

    . /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
    conda activate igwn-py37
    ecp-get-cert -i login.ligo.org -u james.clark

Reconfigure and probe the LIGO repo:

    sudo cvmfs_config setup
    cvmfs_config probe ligo.osgstorage.org

    
Notes:

* Can only probe the ligo data repo with an active x509 proxy
* Each time you add a repo, do `sudo cvmfs_config setup`

## Conda
Test

## Firefox

Sign-in with jclark.astro@gmail.com and sync

## Regolith

[installation instructions](https://regolith-linux.org/docs/getting-started/install/):

    sudo add-apt-repository ppa:regolith-linux/release
    sudo apt install regolith-desktop-standard # or regolith-desktop-mobile for laptops
    sudo apt install regolith-look-gruvbox # setup theme

Issue commands (shift-special-space):

    regolith-look set gruvbox
    regolith-look refresh

Find regolith bar items:

    sudo apt search ^i3xrocks-

Install extras:

    sudo apt install -y i3xrocks-volume i3xrocks-weather i3xrocks-temp i3xrocks-memory i3xrocks-battery i3xrocks-disk-capacity
    regolith-look refresh

Local config in `~/.config/regolith/Xresources`:

    gnome.wallpaper:
    i3-wm.bar.position: top
    i3-wm.gaps.inner.size: 10


## Curl

Really??

    sudo apt install -y curl

## xclip

    sudo apt install -y xclip

## SSH server

    sudo apt install -y openssh-server

## atop, htop

    sudo apt install -y atop htop

## pet command line snippet manager

    wget https://github.com/knqyf263/pet/releases/download/v0.3.0/pet_0.3.0_linux_amd64.deb
    sudo dpkg -i pet_0.3.0_linux_amd64.deb

## Python

    sudo apt instal python3-venv
    sudo apt install python-is-python3

## VPN Client

Get Cisco AnyConnect from Caltech:

    https://www.imss.caltech.edu/services/wired-wireless-remote-access/Virtual-Private-Network-VPN
